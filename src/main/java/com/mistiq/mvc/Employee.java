package com.mistiq.mvc;

import com.mistiq.mvc.validation.CheckEmail;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;

public class Employee {

    @Size(min = 2, message = "name must be min 2 symbols") //размер поля между заданными размерами
    private String name;
    //@NotNull(message = "surname is required field")
    //@NotEmpty(message = "surname is required field") // поле не должно быть пустым
    @NotBlank(message = "surname is required field") // поле не должно быть пустым и не должно быть заполнено только пробелами
    private String surname;
    @Min(value = 500, message = "must be greater than 499")
    @Max(value = 1000, message = "must be less than 1001")
    private int salary;
    private String department;
    private Map<String, String> departmentMap;
    private String carBrand;
    private Map<String, String> carBrandMap;
    private String[] languages;
    private Map<String, String> languagesMap;

    @Pattern(regexp = "\\d(3)-\\d(2)-\\d(2)", message = "Please use pattern XXX-XX-XX")
    private String phoneNumber;

    @CheckEmail(value = "abc.com", message = "email must ends abc.com")
    private String email;

    public Employee() {
        departmentMap = new HashMap<>();
        departmentMap.put("IT", "Information Technology");
        departmentMap.put("HR", "Human Resources");
        departmentMap.put("Sales", "Sales");

        carBrandMap = new HashMap<>();
        carBrandMap.put("BMW","BMW");
        carBrandMap.put("AUDI","AUDI");
        carBrandMap.put("Mercedes-Benz","MB");

        languagesMap = new HashMap<>();
        languagesMap.put("English", "EN");
        languagesMap.put("Deutch", "DE");
        languagesMap.put("Russian", "RU");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Map<String, String> getDepartmentMap() {
        return departmentMap;
    }

    public void setDepartmentMap(Map<String, String> departmentMap) {
        this.departmentMap = departmentMap;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public Map<String, String> getCarBrandMap() {
        return carBrandMap;
    }

    public void setCarBrandMap(Map<String, String> carBrandMap) {
        this.carBrandMap = carBrandMap;
    }

    public String[] getLanguages() {
        return languages;
    }

    public void setLanguages(String[] languages) {
        this.languages = languages;
    }

    public Map<String, String> getLanguagesMap() {
        return languagesMap;
    }

    public void setLanguagesMap(Map<String, String> languagesMap) {
        this.languagesMap = languagesMap;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", salary=" + salary +
                ", department='" + department + '\'' +
                '}';
    }
}
