package com.mistiq.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller //разновидность Component
@RequestMapping("/employee") // Controller mapping
public class MyController {

    @RequestMapping("/") // Method mapping // http://localhost:8080/spring_mvc/
    public String showFirstView() {
        return "first-view";
    }

    @RequestMapping("/askDetails") // http://localhost:8080/spring_mvc/askDetails
    public String askEmployeeDetails(Model model) {

//        Employee emp = new Employee();
//        emp.setName("Ivan");
//        emp.setSurname("Ivanov");
//        emp.setSalary(500);
//        model.addAttribute("employee", emp);

        model.addAttribute("employee", new Employee());

        return "ask-emp-details-view";
    }

    @RequestMapping("/showDetails") // http://localhost:8080/spring_mvc/showDetails
    public String showEmpDetails(@Valid @ModelAttribute("employee") Employee employee, BindingResult bindingResult) {
        /* @Valid означает что атрибут должен пройти процусс валидации
        * BindingResult - содержит результат этой валидации
        * <form:errors path="name"/> - <form:errors/> - форма отображающая ошибки*/

        // @ModelAttribute дает доступ к конуретному атрибуту Model

//        String name = employee.getName();
//        employee.setName("Mr " + name);
//
//        String surname = employee.getSurname();
//        employee.setSurname(surname + "!");
//
//        int salary = employee.getSalary();
//        employee.setSalary(salary*10);
        if (bindingResult.hasErrors()) {
            return "ask-emp-details-view";
        } else {
            return "show-emp-details-view";
        }
    }

//
//    @RequestMapping("/showDetails") // http://localhost:8080/spring_mvc/showDetails
//    public String showEmpDetails() {
//        return "show-emp-details-view";
//    }


//    @RequestMapping("/showDetails") // http://localhost:8080/spring_mvc/showDetails
//    public String showEmpDetails(HttpServletRequest request, Model model) {
//        String empName = request.getParameter("employeeName");
//        empName = "Mr. " + empName;
//
//        model.addAttribute("nameAttribute", empName); // 1. имя атрибута  2. значение атрибута
//        model.addAttribute("description", " - super cool dude");
//
//        return "show-emp-details-view";
//    }

//    @RequestMapping("/showDetails") // http://localhost:8080/spring_mvc/showDetails
//    public String showEmpDetails(@RequestParam("employeeName") String empName, Model model) {
//
//        empName = "Mr. " + empName;
//
//        model.addAttribute("nameAttribute", empName); // 1. имя атрибута  2. значение атрибута
//        model.addAttribute("description", " - super cool dude");
//
//        return "show-emp-details-view";
//    }
}
