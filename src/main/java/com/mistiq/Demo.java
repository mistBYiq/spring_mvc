package com.mistiq;

public class Demo {
    /* MVC (Model View Controller)
    * Model - компонент, отвечающий за данные приложения и за работу с этими данными
    * View(представление) - компонент, отвечающий за взаимодействие с пользователем.
    * Отображает данные и определяет внешний вид приложения
    * Controller - компонент отвечающий за связь между View и Model.
    * Именно здесь сосредоточена логика работы приложения (бизнес логика)
    * */

    /* Spring MVC
    * Spring MVC - это фреймворк для создания web приложений на Java,
    * в основе которого лежит шаблон проектирования MVC
    *
    * Front Controller также известен под именем DispatcherServlet
    *
    * Controller - центр управления Spring MVC
    *
    * Model - контейнер для хранения данных
    *
    * View - web page, которую можно создать с помощью html, jsp, Thymeleaf и т.д.
    * Часто при отображении View используются данные из Model
    *
    * JSP - Java Server Page. Представляет собой html код с небольшим добавлением кода Javad
    *
    * JSTL - Java Server Pages Standard Tag Library. Это расширение спецификации JSP
    * */

    /* Из чего будет состоять Spring MVC приложение
    * 1. Конфигурация Spring
    * 2. Описание Spring bean
    * 3. Web страницы
    * */

    /* Конфигурация Spring MVC
    * 1. Создаём Maven project(можно использовать archetype-webapp например)
    * 2. Добавляем зависимости в pom файл
    * 3. Подключаем Tomcat
    * 4. Добавляем папки/пакеты в иерархию классов
    * 5. Конфигурируем web.xml
    * 6. Конфигурируем applicationContext
    * */

    /* Ambiguous mapping
    * при одинаковых мапингах в сонтроллерах возникае ошибка Ambiguous mapping */

    /* Валидация форм Spring MVC
    * Java Standard Bean Validation - это спецификация, котрая описывает правила валидации
    * Hibernate Validator - реализация правил, описанных в Java Standard Bean Validation
    * */
}
